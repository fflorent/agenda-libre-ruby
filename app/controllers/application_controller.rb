# The top level controller, where can be centralised almost everything
class ApplicationController < ActionController::Base
  before_action :set_paper_trail_whodunnit, :discard
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery prepend: true, with: :exception

  preserve :region, :tag, :near

  layout :handle_xhr_layout

  private

  def set_locale
    I18n.locale =
      http_accept_language.compatible_language_from I18n.available_locales
  end

  # Mechanism to manage filter resets
  def discard
    params.keys.keep_if { |key| params[key].empty? }
          .each { |key| session.delete ['application', key].join '_' }
  end

  protected

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  # If the request is an xhr, we don't render any layout
  def handle_xhr_layout
    request.xhr? ? false : 'application'
  end
end
