ActiveAdmin.register User do
  permit_params :login, :password, :email, :lastname, :firstname

  form do |f|
    f.inputs do
      f.input :login
      f.input :lastname
      f.input :firstname
      f.input :email
      f.input :password
    end
    f.actions
  end
end
