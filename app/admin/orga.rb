ActiveAdmin.register Orga do
  permit_params :kind_id, :name, :description, :url, :city, :department,
                :region_id, :active

  scope :active
  scope :moderated
  scope :unmoderated

  config.sort_order = 'updated_at_desc'

  filter :kind, as: :check_boxes
  filter :name
  filter :region

  index do
    column :kind
    column :name
    column :city
    column :department
    column :region
    column :url
    column :feed
    column :contact
    column :updated_at
    actions
  end

  show do |l|
    attributes_table do
      row :kind
      row :name
      row :description
      row :city
      row :department
      row :region
      row :url do
        link_to l.url, l.url
      end
      row :feed do
        link_to l.feed, l.feed
      end
      row :contact do
        mail_to l.contact, l.contact
      end
      row :active
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :kind
      f.input :name
      f.input :city
      f.input :department
      f.input :region
      f.input :description
      f.input :url
      f.input :feed
      f.input :contact
      f.input :active
    end
    f.actions
  end
end
