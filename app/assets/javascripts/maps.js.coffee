# Setting up OpenStreeMap from a generic #map element
$(document).on 'turbolinks:load', ->
  markerColors = ['blue', 'red', 'darkred', 'orange', 'green', 'darkgreen', 'purple', 'darkpuple', 'cadetblue']
  idx = 0
  $('#map.list').each ->
    map = L.map 'map'

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a>'
    ).addTo map

    controls = L.control.layers(null, null, collapsed: false).addTo map

    $('li a', this).each ->
      url = $(this).attr 'href'
      text = $(this).html()
      markerColor = markerColors[idx++ %% markerColors.length]
      if location.search && url.indexOf('?') >= 0
        url += '&' + location.search.substr 1
      else
        url += location.search

      $.getJSON url, (json) ->
        if json
          layer = L.markerClusterGroup(maxClusterRadius: 30).addLayer L.geoJson json,
            pointToLayer: (feature, latlng) ->
              # Marker with the proper icon
              marker = L.AwesomeMarkers.icon
                prefix: 'fa',
                icon: feature.properties.icon || 'calendar',
                markerColor: markerColor
              L.marker latlng, icon: marker
            ,
            onEachFeature: (feature, layer) ->
              # Does this feature have a property named popupContent?
              if (feature.properties && feature.properties.popupContent)
                layer.bindPopup feature.properties.popupContent

          map.addLayer layer
          controls.addOverlay layer, text + ' - ' + json.length

          if (/maps\//.test(location.href) || /maps.json/.test url) && layer.getBounds()._northEast && layer.getBounds()._southWest
            # Automatic focus to all displayed events
            map.fitBounds layer.getBounds()

  $('#map.event, #map.orga').each ->
    coord = [$(this).data('latitude'), $(this).data('longitude')]

    map = L.map('map').setView [coord[0], coord[1]], 16

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a>'
    ).addTo map

    url = $(this).data 'url'
    markerColor = markerColors[idx++ %% markerColors.length]
    if location.search && url.indexOf('?') >= 0
      url += '&' + location.search.substr 1
    else
      url += location.search

    # Marker with the proper icon
    marker = L.AwesomeMarkers.icon
      prefix: 'fa',
      icon: 'calendar'
    L.marker([coord[0], coord[1]], icon: marker).addTo map

    $.getJSON url, (json) ->
      layer = L.markerClusterGroup(maxClusterRadius: 30).addLayer L.geoJson json,
        pointToLayer: (feature, latlng) ->
          # Marker with the proper icon
          marker = L.AwesomeMarkers.icon
            prefix: 'fa',
            icon: feature.properties.icon || 'calendar',
            markerColor: markerColor
          L.marker latlng, icon: marker
        ,
        onEachFeature: (feature, layer) ->
          # Does this feature have a property named popupContent?
          if (feature.properties && feature.properties.popupContent)
            layer.bindPopup(feature.properties.popupContent)

      map.addLayer layer
