# Preview all emails at http://localhost:3000/rails/mailers/moderation_mailer
class ModerationMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/moderation_mailer/create
  def create
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    ModerationMailer.create Event.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderation_mailer/update
  def update
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    event = Event.first

    event.start_time += 1.day
    event.description = event.description + '
hello world'

    ModerationMailer.update event
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderation_mailer/accept
  def accept
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    ModerationMailer.accept Event.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/moderation_mailer/destroy
  def destroy
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    event = Event.last
    event.reason = 'hello world'
    ModerationMailer.destroy event
  end
end
