# The relation between orga and their kind
class AddDataToOrgas < ActiveRecord::Migration
  def change
    add_reference :orgas, :kind, index: true
    add_foreign_key :orgas, :kinds
    add_column :orgas, :feed, :string
    add_column :orgas, :contact, :string
    add_column :orgas, :submitter, :string
    add_column :orgas, :moderated, :boolean, default: false
    add_column :orgas, :submission_time, :datetime
    add_column :orgas, :decision_time, :datetime
    add_column :orgas, :secret, :string
    add_column :orgas, :deleted, :boolean, default: false, null: false
  end
end
