# Event url are now optional
class ChangeEventsNullUrl < ActiveRecord::Migration
  def change
    change_column :events, :url, :string, default: '', null: true
  end
end
