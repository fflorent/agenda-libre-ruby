# Add a translations table, to manage many texts from the database
class CreateTranslation < ActiveRecord::Migration
  def change
    # rubocop:disable Rails/CreateTableWithTimestamps
    create_table :translations do |t|
      t.string :locale
      t.string :key
      t.text :value
      t.text :interpolations
      t.boolean :is_proc, default: false
    end
    # rubocop:enable Rails/CreateTableWithTimestamps
  end
end
